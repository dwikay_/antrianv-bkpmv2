<?php

Route::get('/', 'APIController@welcome')->name('verifyToken');
Route::get('check-oss/{token}', 'APIController@checkOss')->name('verifyToken');
Route::get('verifyToken/{token}/{idSubLayanan}/{tipe}', 'APIController@verifyToken')->name('verifyToken');


//ajax optional
Route::get('getDate/{subLayanan}/{quota}', 'APIController@getTanggal')->name('registrasi');
Route::get('check-available/{email}/{tanggalKonsultasi}', 'RegistrasiController@checkKetersediaan')->name('registrasi');
Route::get('checkAPI/{token}', 'APIController@checkAPI')->name('verifyToken');

//route perseorangan
Route::post('pendaftaran/storePerserorangan', 'RegistrasiController@storePerserorangan')->name('registrasi');
Route::get('pendaftaran/perseorangan/{data}', 'APIController@perseorangan')->name('perseorangan');
Route::get('pendaftaran/perseorangan/sentToEmail/{idDaftar}', 'RegistrasiController@sukses')->name('sukses');

//Badan Usaha
Route::get('pendaftaran/badanUsaha/{token}', 'BadanUsahaController@badanUsaha')->name('badanUsaha');
Route::get('pendaftaran/badanUsaha/sentToEmail/{idDaftar}', 'BadanUsahaController@sukses')->name('sukses');
Route::post('pendaftaran/storeBadanUsaha', 'BadanUsahaController@storeBadanUsaha')->name('storeBadanUsaha');

//Daerah
Route::get('pendaftaran/daerah/{token}', 'DaerahController@daerah')->name('daerah');
Route::get('pendaftaran/daerah/sentToEmail/{idDaftar}', 'DaerahController@sukses')->name('sukses');
Route::post('pendaftaran/storeDaerah', 'DaerahController@storeDaerah')->name('storeDaerah');

//Kementrian
Route::get('pendaftaran/kementrian/{token}', 'KementrianController@kementrian')->name('daerah');
Route::get('pendaftaran/kementrian/sentToEmail/{idDaftar}', 'KementrianController@sukses')->name('sukses');
Route::post('pendaftaran/storeKementrian', 'KementrianController@storeKementrian')->name('storeKementrian');

//KEK
Route::get('pendaftaran/kek/{token}', 'KekController@kek')->name('daerah');
Route::get('pendaftaran/kek/sentToEmail/{idDaftar}', 'KekController@sukses')->name('sukses');
Route::post('pendaftaran/storeKek', 'KekController@storeKek')->name('storeKek');

// KLOTER
Route::get('getKloterByIdSubLayanan/{idSubLayanan}', 'Kloter\KloterController@getKloterByIdSubLayanan')->name('getKloterByIdSubLayanan');
Route::get('getTotalRegistrasi/{timeNow}/{idSubLayanan}', 'RegistrasiController@getTotalRegistrasi')->name('getTotalRegistrasi');
Route::get('getSubLayananById/{idSubLayanan}', 'RegistrasiController@getSubLayananById')->name('getSubLayananById');

//open meet
Route::get('open-meet/{id}', 'APIController@checkOpenMeet')->name('virtual');
Route::get('open-meet-failed', 'APIController@openMeetFailed')->name('virtual');
Route::get('invalid-link', 'APIController@invalidLink')->name('virtual');
