<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Model\Token as Token;
use App\Library\CurlGen;

class TokenBackend extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'token:backend';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get Token from API Backend';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(CurlGen $curlGen)
    {
      date_default_timezone_set("Asia/Jakarta");
      $url = "/api/authenticate";
      $user = "admin";
      $passwordString = "intimulti";
      $old_token = Token::where('aplikasi','management')->first();

      $data = array(
          'password' => $passwordString,
          'rememberMe' => true,
          'username' => $user
        );

      $param = $curlGen->getToken($url, $data, $old_token->token);

      // print_r($url);
      // print_r($data);
      // print_r($old_token->token);
      // print_r($param);

      $token = Token::where('aplikasi','management')->first();
      $token->token = $param[1]->id_token;
      $token->save();
    }
}
