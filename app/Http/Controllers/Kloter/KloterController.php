<?php

namespace App\Http\Controllers\Kloter;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Library\CurlGen;

class KloterController extends Controller
{
    public function getKloterByIdSubLayanan(CurlGen $curlGen , $idSubLayanan)
    {
    	$url = "/services/antriancoresvc/api/simple-jadwal-masters/getByIdSubLayanan/".$idSubLayanan;
      	$objek = $curlGen->getIndex($url);

      	return $objek;
    }
}
