<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Jerry\JWT\JWT;
use Artisan;
use App\Library\CurlGen;
use Redirect;

class APIController extends Controller
{

  public function openMeetFailed(){
      return view('openMeetFailed');
    }

    public function invalidLink(){
        return view('invalid');
      }

    public function checkOpenMeet(CurlGen $curlGen, $id){

      $pathCekUserAvail = "/services/antriancoresvc/api/pendaftarans/".$id;
      $paramCekUserAvail = $curlGen->getIndex($pathCekUserAvail);

      if($paramCekUserAvail[0] == "404"){
        return redirect(url('invalid-link'));
      }
      $pathLinkVirtual = "/services/antriancoresvc/api/link-virtuals/getByTanggalAndIdSubLayanan/".$paramCekUserAvail[1]['tanggalPengambilan']."/".$paramCekUserAvail[1]['idSubLayanan'];
      $paramLinkVirtual = $curlGen->getIndex($pathLinkVirtual);

      // dd($paramCekUserAvail);
      if($paramLinkVirtual[0] == "404"){
        return redirect(url('open-meet-failed'));
      }else{

        return view('openMeet')
        ->with('paramLinkVirtual', $paramLinkVirtual[1]);
      }
    }

  public function getTanggal($subLayanan, $quota){

          $url = "http://10.1.237.145/new/havingcountantrian/public/getDate/".$subLayanan."/".$quota;

          $ch = curl_init();
          curl_setopt($ch, CURLOPT_URL, $url);
          curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
          curl_setopt($ch, CURLOPT_HEADER, 0);
          curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
          curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
          curl_setopt($ch, CURLOPT_TIMEOUT, 4000);
          $output = curl_exec($ch);
          $info = curl_getinfo($ch);
          $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

          curl_close($ch);

          $param = json_decode($output, true);
          //dd($url);
          //return $param[0];
          if($param != null){
            if($param[0]['hasil'] == []){
            $params =  array(
                "hasil" => ['1978-02-19'],
                "jumlah" => 0
              );
            }else{
              $params = $param[0];
            }

          }else{
            $params =  array(
              "hasil" => ['1978-02-19'],
              "jumlah" => 0
            );

          }
          return $params;

      }

  public function checkAPI($token){

    $tokenParts = explode(".", $token);
    $tokenHeader = base64_decode($tokenParts[0]);
    $tokenPayload = base64_decode($tokenParts[1]);
    $jwtHeader = json_decode($tokenHeader);
    $jwtPayload = json_decode($tokenPayload);
    dd($jwtPayload->data);

  }
  public function welcome(CurlGen $curlGen){

      // return Redirect::to("https://oss.go.id");
      return view('home');
      // Artisan::call("token:backend");
      //
      // $pathLayanan = "/services/antriancoresvc/api/sub-layanans/getLayananOnline/true";
      // $paramLayanan = $curlGen->getIndex($pathLayanan);
      //
      // // dd($paramLayanan[1]);
      // return view('welcome')
      // ->with('layanan', $paramLayanan[1]);
  }


  public function checkOss(CurlGen $curlGen,$token)
  {

    Artisan::call("token:backend");

    $pathLayanan = "/services/antriancoresvc/api/sub-layanans/getLayananOnline/true";
    $paramLayanan = $curlGen->getIndex($pathLayanan);

    // dd($paramLayanan[1]);
    return view('welcomeToken')
    ->with('keysp',$token)
    ->with('layanan', $paramLayanan[1]);

  }
  public function verifyToken(CurlGen $curlGen,$token, $idSubLayanan, $tipe){

    // $payload = JWT::decode($token);
    // print_r($payload);

    $tokenParts = explode(".", $token);
    $tokenHeader = base64_decode($tokenParts[0]);
    $tokenPayload = base64_decode($tokenParts[1]);
    $jwtHeader = json_decode($tokenHeader);
    $jwtPayload = json_decode($tokenPayload);
    // dd($tipe);

    if($jwtPayload->data->role == "11"){
      // PELAKU USAHA
      if($jwtPayload->data->jenis_perseroan == "17"){

        $data = array(
          "nik" => $jwtPayload->data->nomor_identitas,
          "nama" => $jwtPayload->data->nama,
          "email" => $jwtPayload->data->email,
          "tipe" => $tipe,
          "tokenJwt" => $token,
          "idSubLayanan" => $idSubLayanan
        );
        $jsons = json_encode($data);

        return redirect(url('pendaftaran/perseorangan/'.base64_encode($jsons)));

      }else{

        $paramPerusahaan = $curlGen->getNamaPerusahaan($jwtPayload->data->id_profile);

        $data = array(
          "nik" => $jwtPayload->data->nomor_identitas,
          "nama" => $jwtPayload->data->nama,
          "namaBadanUsaha" => $paramPerusahaan[1]['dataNib'][0]['nama_perusahaan'],
          "email" => $jwtPayload->data->email,
          "tipe" => $tipe,
          "tokenJwt" => $token,
          "idSubLayanan" => $idSubLayanan
        );
        $jsons = json_encode($data);
        return redirect(url('pendaftaran/badanUsaha/'.base64_encode($jsons)));
      }
    } else if($jwtPayload->data->role == "07" || $jwtPayload->data->role == "08" || $jwtPayload->data->role == "68"){

      if($idSubLayanan == "3509"){
        $subLayananId = "125401";
      }else{
        $subLayananId = $idSubLayanan;
      }
      // dd($jwtPayload->data);
      if($jwtPayload->data->role=="07"){$unit = "KL - Admin";}else if($jwtPayload->data->role=="08"){$unit = "KL - Unit Perizinan";}else{$unit = "KL - Unit Pengawasan";}
      $data = array(
        "unit" => $unit,
        "instansi" => $jwtPayload->data->kantor,
        "nik" => $jwtPayload->data->nomor_identitas,
        "namaPejabat" => $jwtPayload->data->nama,
        "email" => $jwtPayload->data->email,
        "tipe" => $tipe,
        "tokenJwt" => $token,
        "idSubLayanan" => $subLayananId
      );
      $jsons = json_encode($data);
      return redirect(url('pendaftaran/kementrian/'.base64_encode($jsons)));

    } else if($jwtPayload->data->role == "10" || $jwtPayload->data->role == "20" || $jwtPayload->data->role == "22" || $jwtPayload->data->role == "24" || $jwtPayload->data->role == "78"){

      $paramPerusahaan = $curlGen->getNamaPerusahaan($jwtPayload->data->id_profile);
      // dd($paramPerusahaan);
      // dd($jwtPayload->data);
      if($idSubLayanan == "3509"){
        $subLayananId = "125401";
      }else{
        $subLayananId = $idSubLayanan;
      }
      $data = array(
        "unit" => $jwtPayload->data->nama,
        "instansi" => $jwtPayload->data->kantor,
        "namaPejabat" => "",
        "email" => $jwtPayload->data->email,
        "tipe" => $tipe,
        "tokenJwt" => $token,
        "idSubLayanan" => $subLayananId
      );
      $jsons = json_encode($data);
      return redirect(url('pendaftaran/daerah/'.base64_encode($jsons)));

    } else if($jwtPayload->data->role == "17"){

      if($idSubLayanan == "3509"){
        $subLayananId = "125401";
      }else{
        $subLayananId = $idSubLayanan;
      }
      $paramPerusahaan = $curlGen->getNamaPerusahaan($jwtPayload->data->id_profile);
      // dd($paramPerusahaan[1]['dataNib'][0]['nama_perusahaan']);

      $data = array(
        "unit" => $jwtPayload->data->kantor,
        "instansi" => $jwtPayload->data->nama,
        "namaPejabat" => $paramPerusahaan[1]['dataNib'][0]['nama_perusahaan'],
        "email" => $jwtPayload->data->email,
        "nik" => "",
        "tipe" => $tipe,
        "tokenJwt" => $token,
        "idSubLayanan" => $subLayananId
      );
      $jsons = json_encode($data);
      return redirect(url('pendaftaran/kek/'.base64_encode($jsons)));

    }else{
      dd("ROLE TIDAK DITEMUKAN");
    }
  }

  public function perseorangan(CurlGen $curlGen,$data){

    $decodes = base64_decode($data);
    $jsons = json_decode($decodes);

      $tanggalHariLibur = "";
      $urlHariLibur = "/services/antriancoresvc/api/hari-liburs?size=999999";
      $getHariLibur = $curlGen->getIndex($urlHariLibur);

      if($getHariLibur[0] == 200){
        foreach($getHariLibur[1] as $key => $value){
          $tanggalHariLibur = $tanggalHariLibur.','.$value['tanggal'];
        }
      }
      // dd($jsons);

      $pathLayanan = "/services/antriancoresvc/api/simple-sub-layanans/".$jsons->idSubLayanan;
      $paramLayanan = $curlGen->getIndex($pathLayanan);
      // dd($paramLayanan);
    return view('pendaftaran.akses.perseorangan')
    ->with('nama', $jsons->nama)
    ->with('nik', $jsons->nik)
    ->with('keysp',$data)
    ->with('pelayanan', $paramLayanan[1])
    ->with('hariLibur', substr($tanggalHariLibur,1))
    ->with('idSubLayanan',$jsons->idSubLayanan)
    ->with('tipe', $jsons->tipe)
    ->with('email', $jsons->email);
  }


}
