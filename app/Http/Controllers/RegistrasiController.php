<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Jerry\JWT\JWT;
use Artisan;
use App\Library\CurlGen;
use Webpatser\Uuid\Uuid;
use File;
use Mail;


class RegistrasiController extends Controller
{

  public function checkKetersediaan(CurlGen $curlGen, $email, $tanggalKonsultasi){

    $urlData = "/services/antriancoresvc/api/pendaftarans/checkAvilable/".$email."/".$tanggalKonsultasi;
    $cekData = $curlGen->getIndex($urlData);

    return $cekData;

  }
  function generateRandomString($length = 10) {
    return substr(str_shuffle(str_repeat($x='ABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil($length/strlen($x)) )),1,$length);
  }

    public function storePerserorangan(CurlGen $curlGen, Request $request){

      date_default_timezone_set('asia/jakarta');
      $tgl = date('Y-m-d');
      $tglKonsultasi = $request->tanggal_konsultasi;
      $newDateKonsultasi = date("Y-m-d", strtotime($tglKonsultasi));

      $weekDay = date('w', strtotime($tglKonsultasi));
      $hariLibur = $request->tanggal_merah;

      $jumlahKuota = $request->jumlah_kuota;
      $kuotaTerdaftar = $request->kuota_terdaftar;

      if($weekDay == 0 || $weekDay == 6){
        $info = "error";
        $alert = "Tidak bisa registrasi pada hari weekend!";
        return redirect()->back()
        ->with('info', $info)
        ->with('alert', $alert);
      }

      if(in_array($tglKonsultasi, $hariLibur)) {
          $info = "error";
          $alert = "Tidak bisa registrasi pada hari libur!";
          return redirect()->back()
          ->with('info', $info)
          ->with('alert', $alert);
      };
      if($kuotaTerdaftar >= $jumlahKuota) {
          $info = "error";
          $alert = "Kuota hari ini telah penuh!";
          return redirect()->back()
          ->with('info', $info)
          ->with('alert', $alert);
      };

      // dd($request->all());
      $uid4 = Uuid::generate();
      $randomString = str_random(255);

      $rounds =  round(microtime(true) * 1000);
      $kode = substr($rounds, 7);
			$hasilKode = $this->generateRandomString(2).''.substr($rounds, 9);
      $unggah = $request->file('filePermasalahan');
      // dd(count($request->file('filePermasalahan')));

      $data = array(
        "agent" => false,
        "email" => $request->email,
        "idDaftar" => $uid4->string,
        "idKloter" => $request->idKloter,
        "idSubLayanan" => $request->id_jenis_layanan,
        "instansi" => null,
        "jabatan" => null,
        "jamKloter" => $request->kloter.' ('.$request->jamKloterAwal." - ".$request->jamKloterAkhir.'  WIB)',
        "jamLayanan" => $request->jamKloterAwal,
        "jawaban" => null,
        "jwtToken" => $request->keysp,
        "kodeKonfirmasi" => $hasilKode,
        "namaBadanUsaha" => null,
        "namaDireksi" => null,
        "namaPejabat" => null,
        "namaPerseorangan" => $request->nama,
        "nik" =>  $request->nik,
        "noTiket" => null,
        "pengambilanStruk" => false,
        "pertanyaan" =>  $request->pertanyaan,
        "statusTiket" => 0,
        "jenisKendala" => $request->jenisKendala,
        "tanggalPengambilan" => $request->tanggalKonsultasi,
        "tipeKtp" => null,
        "typeVirtual" => true,
        "unit" => null,
        "flagBanned" => 0,
        "untukLayanan" => $request->kategori
      );

      $urlSaveRegistrasi = "/services/antriancoresvc/api/pendaftarans";
      $paramSaveRegistrasi = $curlGen->store($urlSaveRegistrasi, $data);

      if($unggah){
        foreach ($unggah as $key => $file) {
          // dd($file);
          $destinationPath = "file-permasalahan/".$request->nik.str_random(4).'-'.date('Y-m-d');
            if (!is_dir($destinationPath)) {
                 File::makeDirectory(public_path()
                    .
                    '/'.$destinationPath, 0777, true);
              }
            $fileName = $file->getClientOriginalName();
            $fileExt = $file->getClientOriginalExtension();
            $file->move(public_path($destinationPath), $file->getClientOriginalName());

            $img = $destinationPath."/".$fileName;

            $dataImage = array(
              "idSubLayanan" => $request->id_jenis_layanan,
              "namaFile" => $fileName,
              "pathFile" => $img,
              "relasiId" => $paramSaveRegistrasi[1]['id']
            );

            $urlImage = "/services/antriancoresvc/api/image-repositories";
            $paramImage = $curlGen->store($urlImage, $dataImage);

        }
      }


      if($paramSaveRegistrasi[0] != 201){

        $data = array(
          "nik" => $request->nik,
          "nama" => $request->nama,
          "email" => $request->email,
          "tipe" => $request->tipe,
          "idSubLayanan" => $request->id_jenis_layanan,
        );
        $jsons = json_encode($data);

        return redirect(url('pendaftaran/perseorangan'.base64_encode($jsons)));
      }


      return redirect(url('pendaftaran/perseorangan/sentToEmail/'.$paramSaveRegistrasi[1]['id']));
    }

    public function sukses(CurlGen $curlGen,$idDaftar){


      $urlPendaftaran = "/services/antriancoresvc/api/pendaftarans/".$idDaftar;
      $paramDaftar = $curlGen->getIndex($urlPendaftaran);

      $urlImage = "/services/antriancoresvc/api/image-repositories/getImage/".$idDaftar."/". $paramDaftar[1]['idSubLayanan'];
      $images = $curlGen->getIndex($urlImage);
      // dd($paramDaftar[1]);

      $linkZoom = "";
      $paramZoom = $curlGen->getIndex($linkZoom);

      // $newDateKonsultasi = date("Y-m-d", strtotime($paramDaftar['1']['tanggalPengambilan']));
      // $timestamp = strtotime($newDateKonsultasi);
      // $waktu_pengambilan = date('d-M-y', $timestamp);

      $waktu_pengambilan = $this->tanggal_indo($paramDaftar['1']['tanggalPengambilan']);
      // dd($waktu_pengambilan);

      $registrasi = $paramDaftar[1];
      $img = $images[1];

      Mail::send('email.akses.perseorangan.virtual', ['registrasi' => $registrasi, 'waktu_pengambilan' => $waktu_pengambilan], function ($m) use ($registrasi, $img) {
        $m->from('antrianonline@bkpm.go.id', 'Lembaga OSS - Konsultasi');
        // $m->to($registrasi['email'])->subject('Konfirmasi Antrian');
        $m->to('aliansyahkay@gmail.com')->subject('Pendaftaran Konsultasi');
        $m->bcc('farizalamsyah22@gmail.com')->subject('Pendaftaran Konsultasi');
        // $m->bcc('cctokopediahalo@gmail.com')->subject('Konfirmasi Antrian');

        foreach($img as $docs){
          $m->attach(public_path($docs['pathFile']));
        }

      });

      return view('sukses')
      ->with('route', 'perseorangan')
      ->with('tipe', $paramDaftar[1]['untukLayanan'])
      ->with('waktu_pengambilan',$waktu_pengambilan)
      ->with('jamKloter',$paramDaftar[1]['jamKloter'])
      ->with('idDaftar',$paramDaftar[1]['id']);
    }

    public function getTotalRegistrasi(CurlGen $curlGen, $timeNow, $idSubLayanan){
        $url = "/services/antriancoresvc/api/getTotalRegistrasi/".$timeNow."/".$idSubLayanan;
        $objek = $curlGen->getIndex($url);

        return $objek;
      }

      public function getSubLayananById(CurlGen $curlGen, $idSubLayanan){
        $url = "/services/antriancoresvc/api/simple-sub-layanans/".$idSubLayanan;
        $objek = $curlGen->getIndex($url);

        return $objek;
      }

      public function tanggal_indo($tanggal)
          {
          	$bulan = array (1 =>   'Januari',
          				'Februari',
          				'Maret',
          				'April',
          				'Mei',
          				'Juni',
          				'Juli',
          				'Agustus',
          				'September',
          				'Oktober',
          				'November',
          				'Desember'
          			);
          	$split = explode('-', $tanggal);
          	return $split[2] . ' ' . $bulan[ (int)$split[1] ] . ' ' . $split[0];
          }
}
