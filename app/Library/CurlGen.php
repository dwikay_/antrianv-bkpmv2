<?php namespace App\Library;

use File;
use App\User;
use App\Model\Token;

class CurlGen {

  public function getNamaPerusahaan($idProfile){

    $url = "https://203.114.226.150/dev/v1/izin/main/getNamaPerusahaanAccount";

    date_default_timezone_set("Asia/Jakarta");
    $content_type = "application/json";

    $authorization = "c30a3c978abe16107cdaea709b1a7c5f";

    $datas = [
    'dataPermohonan' => [
      "id_profile" => $idProfile
      ]
    ];
    // dd(json_encode($datas));
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($datas));
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
      'user_key: '.$authorization,
      'Content-Type: application/json'
      )
    );
      $output = curl_exec($ch);
      $info = curl_getinfo($ch);
      $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

      curl_close($ch);

      $param = json_decode($output, true);
      // dd($param);
      return [$httpCode,$param];
  }

  public function checkPengajuan($urls){ //GET

    $url = config('custom.api_url').$urls;


    date_default_timezone_set("Asia/Jakarta");
    $content_type = "application/json";
    $user = Token::where('aplikasi','registrasi')->orderby('id','desc')->first();
    $authorization = $user->token;

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($ch, CURLOPT_TIMEOUT, 4000);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
      'Authorization: Bearer '.$authorization
      )
    );
      $output = curl_exec($ch);
      $info = curl_getinfo($ch);
      $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

      curl_close($ch);

      $param = json_decode($output, true);

      return [$httpCode,$param];
  }

  public function kseiDoc($urls, $kodeNasabah){

    $url = config('custom.api_url').$urls;


    date_default_timezone_set("Asia/Jakarta");
    $content_type = "application/xml";
    $user = Token::orderby('id','desc')->first();
    $authorization = $user->token;

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($ch, CURLOPT_TIMEOUT, 4000);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
      'Content-Type: '.$content_type,
      'Authorization: Bearer '.$authorization
      )
    );
      $output = curl_exec($ch);
      $info = curl_getinfo($ch);
      $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
      // return $urls;
      $XmlElement = simplexml_load_string($output);

      curl_close($ch);

      $xmlDoc = new SimpleXMLElement($output);
      $xmlDoc->saveXML($kodeNasabah.'.xml');

      $response = Response::make($xmlDoc->asXML(), 200);
      $response->header('Cache-Control', 'public');
      $response->header('Content-Description', 'File Transfer');
      $response->header('Content-Disposition', 'attachment; filename='.$kodeNasabah.'.xml');
      $response->header('Content-Transfer-Encoding', 'binary');
      $response->header('Content-Type', 'text/xml');

      return $response;
  }

  public function kseiDocCsv($urls, $kodeNasabah){

    $url = config('custom.api_url').$urls;


    date_default_timezone_set("Asia/Jakarta");
    $content_type = "application/xml";
    $user = Token::orderby('id','desc')->first();
    $authorization = $user->token;

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($ch, CURLOPT_TIMEOUT, 4000);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
      'Content-Type: '.$content_type,
      'Authorization: Bearer '.$authorization
      )
    );
      $output = curl_exec($ch);
      $info = curl_getinfo($ch);
      $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
      // return $urls;

      curl_close($ch);

      $param = json_decode($output, true);

      return $output;
  }

  public function getToken($urls,$data, $old_token){

    $url = config('custom.api_url').$urls;

    date_default_timezone_set("Asia/Jakarta");

    $datas = json_encode($data);
    $content_type = "application/json";

    $ch = curl_init();
      curl_setopt($ch, CURLOPT_URL, $url);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($ch, CURLOPT_HEADER, 0);
      curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
      curl_setopt($ch, CURLOPT_POST, 1);
      curl_setopt($ch, CURLOPT_POSTFIELDS, $datas);
      curl_setopt($ch, CURLOPT_HTTPHEADER, array(
          'Content-Type: '.$content_type,
          'Authorization: '.$old_token
        )
      );
      $output = curl_exec($ch);
      $info = curl_getinfo($ch);
      $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

      curl_close($ch);

    $asd = json_decode($output);

    // print_r($asd);

    return [$httpCode,$asd];
  }

  public function uploads($urls,$data,$afs){

    ini_set('max_execution_time', 0);

    $url = config('custom.api_url').$urls;

    date_default_timezone_set("Asia/Jakarta");
    $token = Token::orderby('id','desc')->first();

    $datas = $data;
    $content_type = "multipart/form-data";
    $accept = "text/plain";

    // $fp = fopen(public_path().'\\'.str_replace('/','\\',$data['file']), 'rb');
    // $size = filesize(public_path().'\\'.str_replace('/','\\',$data['file']));

    $ch = curl_init();

    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/4.0 (compatible;)");
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Content-Type: '.$content_type,
        'Accept: '.$accept,
        'Authorization: Bearer '.$token->token
      )
    );
    curl_setopt($ch, CURLOPT_FRESH_CONNECT, 1);
    curl_setopt($ch, CURLOPT_FORBID_REUSE, 1);
    curl_setopt($ch, CURLOPT_TIMEOUT, 100);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data);

      $output = curl_exec($ch);
      $info = curl_getinfo($ch);
      $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

      curl_close($ch);

      $asd = json_decode($output, true);

      return [$url,$httpCode,$asd,$datas];

  }

  public function getIndex($urls){ //GET

    $url = config('custom.api_url').$urls;


    date_default_timezone_set("Asia/Jakarta");
    $content_type = "application/json";
    $user = Token::where('aplikasi','management')->first();
    $authorization = $user->token;

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($ch, CURLOPT_TIMEOUT, 4000);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
      'Authorization: Bearer '.$authorization
      )
    );
      $output = curl_exec($ch);
      $info = curl_getinfo($ch);
      $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

      curl_close($ch);

      $param = json_decode($output, true);

      return [$httpCode,$param];
  }

  public function store($urls,$data){ //POST

    $url = config('custom.api_url').$urls;

    date_default_timezone_set("Asia/Jakarta");
    $token = Token::where('aplikasi','management')->first();

    $datas = json_encode($data);
    $content_type = "application/json";

    $ch = curl_init();
      curl_setopt($ch, CURLOPT_URL, $url);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($ch, CURLOPT_HEADER, 0);
      curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
      curl_setopt($ch, CURLOPT_POST, 1);
      curl_setopt($ch, CURLOPT_POSTFIELDS, $datas);
      curl_setopt($ch, CURLOPT_HTTPHEADER, array(
          'Content-Type: '.$content_type,
          'Accept: '.$content_type,
          'Authorization: Bearer '.$token->token
        )
      );
      $output = curl_exec($ch);
      $info = curl_getinfo($ch);
      $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

      curl_close($ch);

      $asd = json_decode($output, true);

      return [$httpCode,$asd];

  }


  public function storeSec($urls,$data){ //POST

    $url = config('custom.api_url').$urls;

    date_default_timezone_set("Asia/Jakarta");
    $token = Token::where('aplikasi','management')->first();

    $datas = json_encode($data);

    $content_type = "application/json";

    $ch = curl_init();
      curl_setopt($ch, CURLOPT_URL, $url);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($ch, CURLOPT_HEADER, 0);
      curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
      curl_setopt($ch, CURLOPT_POST, 1);
      curl_setopt($ch, CURLOPT_POSTFIELDS, $datas);
      curl_setopt($ch, CURLOPT_HTTPHEADER, array(
          'Content-Type: '.$content_type,
          'Accept: '.$content_type,
          'Authorization: Bearer '.$token->token
        )
      );
      $output = curl_exec($ch);
      $info = curl_getinfo($ch);
      $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

      curl_close($ch);

      $asd = json_decode($output, true);

      return [$httpCode,$asd];

  }

  public function storeResView($urls,$data){ //POST

    $url = config('custom.api_url').$urls;

    date_default_timezone_set("Asia/Jakarta");
    $token = Token::orderby('id','desc')->first();

    $datas = json_encode($data);
    $content_type = "application/json";

    $ch = curl_init();
      curl_setopt($ch, CURLOPT_URL, $url);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($ch, CURLOPT_HEADER, 0);
      curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
      curl_setopt($ch, CURLOPT_POST, 1);
      curl_setopt($ch, CURLOPT_POSTFIELDS, $datas);
      curl_setopt($ch, CURLOPT_HTTPHEADER, array(
          'Content-Type: '.$content_type,
          'Accept: '.$content_type,
          'Authorization: Bearer '.$token->token
        )
      );
      $output = curl_exec($ch);
      $info = curl_getinfo($ch);
      $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

      curl_close($ch);

      $asd = json_decode($output, true);

      return [$httpCode,$asd];

  }

  public function edit($urls){ //GET

    $url = config('custom.api_url').$urls;
    date_default_timezone_set("Asia/Jakarta");
    $content_type = "application/json";
    $user = Token::orderby('id','desc')->first();
    $authorization = $user->token;
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($ch, CURLOPT_TIMEOUT, 4000);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
      'Authorization: Bearer '.$authorization
      )
    );
      $output = curl_exec($ch);
      $info = curl_getinfo($ch);
      $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

      curl_close($ch);
      $data = json_decode($output, true);

      return [$httpCode,$data];
  }

  public function update($urls,$data){ //PUT

    $url = config('custom.api_url').$urls;
    date_default_timezone_set("Asia/Jakarta");
    $token = Token::orderby('id','desc')->first();

    $datas = json_encode($data);
    $content_type = "application/json";
    $ch = curl_init();
      curl_setopt($ch, CURLOPT_URL, $url);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($ch, CURLOPT_HEADER, 0);
      curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
      curl_setopt($ch, CURLOPT_POST, 1);
      curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
      curl_setopt($ch, CURLOPT_POSTFIELDS, $datas);
      curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Content-Type: '.$content_type,
        'Accept: '.$content_type,
        'Authorization: Bearer '.$token->token
      )
    );
      $output = curl_exec($ch);
      $info = curl_getinfo($ch);
      $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
      curl_close($ch);
      $param = json_decode($output, true);
      return [$httpCode,$param];
  }
  public function patch($urls,$data){ //PUT

    $url = config('custom.api_url').$urls;
    date_default_timezone_set("Asia/Jakarta");
    $token = Token::orderby('id','desc')->first();

    $datas = json_encode($data);
    $content_type = "application/json";
    $ch = curl_init();
      curl_setopt($ch, CURLOPT_URL, $url);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($ch, CURLOPT_HEADER, 0);
      curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
      curl_setopt($ch, CURLOPT_POST, 1);
      curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PATCH");
      curl_setopt($ch, CURLOPT_POSTFIELDS, $datas);
      curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Content-Type: '.$content_type,
        'Accept: '.$content_type,
        'Authorization: Bearer '.$token->token
      )
    );
      $output = curl_exec($ch);
      $info = curl_getinfo($ch);
      $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
      curl_close($ch);
      $param = json_decode($output, true);
      return [$httpCode,$param];
  }
  public function delete($urls){ //Delete

    date_default_timezone_set("Asia/Jakarta");
    $url = config('custom.api_url').$urls;
    $token = Token::orderby('id','desc')->first();
    $content_type = "application/json";
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Accept: */*',
        'Authorization: Bearer '.$token->token
      )
    );
      $output = curl_exec($ch);
      $info = curl_getinfo($ch);
      $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

      curl_close($ch);
      $asd = json_decode($output, true);

      return $httpCode;
  }

}

?>
