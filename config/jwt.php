<?php
/**
 * Created by PhpStorm.
 * User: gaoyang
 * Date: 2017/11/14
 * Time: 9:32
 */

return [
    "secret" => "MLfyzccBTtCj5JFPdv0Ac6jfRHcqEP3u",         //jwt SHA256 signature use the secret
    "expire_in" => 604800,                                  //jwt expire_in the times(seconds) , default 604800(a week)
];
