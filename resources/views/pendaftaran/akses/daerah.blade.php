@extends('layout')
@section('content')
<div class="row">
  <div class="col"></div>
  <div class="col-md-9" style="background-color: #fff;">
    <div class="row">
      <!-- content -->
      <div class="col-12 p-5">
        <div class="row">
        <div class="col-12">
          <img src="{{asset('home/images/logo_oss.svg')}}" class="logoTop" alt="">
        </div>
        <div class="col-12 mt-3 mb-4">
          <h3>Formulir Pendaftaran Kementerian Lembaga <br>Daerah, KEK dan KPBPB</h3>
        </div>
        <div class="col-md-6 p-3">
            <form enctype="multipart/form-data" id="fDaerah" method="POST" action="{{url('pendaftaran/storeDaerah')}}">
              {{csrf_field()}}
              <div class="mb-3">
                <label for="disabledTextInput" class="form-label">Instansi</label>
                @if($instansi == "")
                <input type="text" name="instansi" id="instansi" class="form-control" placeholder="" value="">
                @else
                <input readonly type="text" name="instansi" id="instansi" class="form-control" placeholder="" value="{{$instansi}}">
                @endif
              </div>
              <div class="mb-3">
                <label for="disabledTextInput" class="form-label">Unit</label>
                @if($unit == "")
                <input type="text" name="unit" id="unit" class="form-control" placeholder="" value="">
                @else
                <input readonly type="text" name="unit" id="unit" class="form-control" placeholder="" value="{{$unit}}">
                @endif

              </div>
              <div class="mb-3">
                <label for="disabledTextInput" class="form-label">Nama Pejabat</label>
                @if($namaPejabat == "")
                <input type="text" name="namaPejabat" id="namaPejabat" class="form-control" placeholder="" value="">
                @else
                <input readonly type="text" name="namaPejabat" id="namaPejabat" class="form-control" placeholder="" value="{{$namaPejabat}}">
                @endif
              </div>
              <div class="mb-3">
                <label for="exampleInputPassword1" class="form-label">Alamat Email</label>
                <input readonly type="email" class="form-control" name="email" id="email" id="" value="{{$email}}">
                <input type="hidden" class="form-control" id="tanggal_merah" name="tanggal_merah[]" value="">

                <!-- KLOTER -->
                <input type="hidden" class="form-control" id="jamKloter" name="idKloter" value="0">
                <input type="hidden" class="form-control" id="jamKloterAwal" name="jamKloterAwal">
                <input type="hidden" class="form-control" id="jamKloterAkhir" name="jamKloterAkhir">
                <input type="hidden" class="form-control" id="jamKloterSekarang" name="jamKloterSekarang">
                <input type="hidden" class="form-control" id="jumlah_kuota" name="jumlah_kuota" value="">
                <input type="hidden" class="form-control" id="kuota_terdaftar" name="kuota_terdaftar">
                <input type="hidden" class="form-control" id="kloter" name="kloter" value="">
                <!-- END KLOTER -->

                <input type="hidden" id="quota" name="quota" value="{{$pelayanan['kuota']}}">
                <input type="hidden" class="form-control" name="id_jenis_layanan" id="id_jenis_layanan" value="{{$idSubLayanan}}">
                <input type="hidden" class="form-control" name="keysp" id="keysp" value="{{$keysp}}">
                <input type="hidden" class="form-control" name="tipe" id="ketipeysp" value="{{$tipe}}">
              </div>
              <div class="mb-3">
                <label for="exampleInputPassword1" class="form-label">Jenis Kendala</label>
                <select class="form-control" name="jenisKendala" id="jenisKendala">
                  <option value="" selected disabled></option>
                  <option value="regulasi">Regulasi</option>
                  <option value="teknis">Teknis</option>
                </select>
              </div>

        </div>
        <div class="col-md-6 p-3">

              <div class="mb-3">
                <label for="exampleInputEmail1" class="form-label">Tanggal Konsultasi <span style="color:red;font-size:7pt">*) tidak tersedia</span>
                  <span style="color:#454545;font-size:7pt">*) tersedia</span>
                </label>
                <input type="text" class="form-control red-b" id="dateJob" name="tanggalKonsultasi" autocomplete="off">
                <input type="text" class="form-control red-b" id="kategori" name="kategori" value="kld" style="display:none">
              </div>

              <div class="mb-3">
                <label for="exampleInputPassword1" class="form-label">Kendala</label>
                <textarea class="form-control red-b" id="pertanyaan" name="pertanyaan" maxlength="100" rows="3"></textarea>
              </div>
              <div class="mb-3">
                <label for="formFile" class="form-label">Unggah gambar kendala</label>
                <input class="form-control" type="file" id="filePermasalahan" name="filePermasalahan[]" multiple="multiple" accept="image/*">
                <small class="info-box" id="noteFile">Maksimal ukuran gambar 2Mb<br>format gambar wajib .jpg atau .png</small>
                <div id="preview" class="mt-3"></div>
              </div>
            </form>
        </div>
        <div class="col-6">
          <a class="btn btn-primary border-0" id="btnsubmit" data-bs-toggle="modal" data-bs-target="#exampleModal">Kirim</a>
        </div>
        </div>
      </div>
    </div>
  </div>
  <div class="col"></div>
</div>
@endsection
@include('pendaftaran.comp.modal-preview')
@section('script')

<script>

var msg = '{{Session::get('alert')}}';
var info = '{{Session::get('info')}}';
var exist = '{{Session::has('alert')}}';
var infoMsg = '{{Session::get('infoMsg')}}';
var infoMsgs = '{{Session::get('infoMsgs')}}';
var rstMsg = infoMsgs+" "+infoMsg;
if(exist){
//swal(rstMsg,msg,info)
$.confirm({
         title: rstMsg,
         content: msg,
         buttons: {
             ok: function () {
             },
         }
     });
}


loadingHide();

function loadingShow() {
$('#loading').css('opacity', '0.7');
$('#loading').css('background-color', '#000');
$('#loading-image').css('display', 'block');
$('#loading').css('display', 'block');
}

function loadingHide() {
$('#loading').css('opacity', '0');
$('#loading').css('background-color', 'as');
$('#loading-image').css('display', 'none');
$('#loading').css('display', 'none');
}

function getDate(){

var date = new Date();
date.getHours();
date.getMinutes();
date.setDate(date.getDate() + 1);


//console.log($.datepicker.formatDate("yy-mm-dd", date));
var resDate = $.datepicker.formatDate("yy-mm-dd", date);

console.log('resDate ' + resDate)
return resDate;

}

function timeNow() {

  var d = new Date();
  var yesterday = new Date();
  h = (d.getHours()<10?'0':'') + d.getHours(),
  m = (d.getMinutes()<10?'0':'') + d.getMinutes();


  var times = h + ':' + m;
  var result;
  var resDates = getDate();

  yesterday.setDate(d.getDate() - 1);
  console.log('Today: ' + d);
  console.log('Yesterday: ' + yesterday);

  mm = '' + (yesterday.getMonth() + 1),
  dd = '' + yesterday.getDate(),
  yy = yesterday.getFullYear();

  mmNow = '' + (d.getMonth() + 1),
  ddNow = '' + d.getDate(),
  yyNow = d.getFullYear();

  console.log('MM LENGHT ' + mm.length)
  console.log('DD LENGHT ' + dd.length)

  if (mm.length < 2) {
    mm = '0' + mm;
  }

  if (dd.length < 2) {
    dd = '0' + dd;
  }

  if (mmNow.length < 2) {
    mmNow = '0' + mmNow;
  }

  if (ddNow.length < 2) {
    ddNow = '0' + ddNow;
  }

  dateYstrd = yy+'-'+mm+'-'+dd+' 12:00'
  dateNows =  yyNow+'-'+mmNow+'-'+ddNow+' '+times
  dateNowz = yyNow+'-'+mmNow+'-'+ddNow
  console.log('yy-mm-dd Yesterday', dateYstrd)
  console.log('yy-mm-dd Now', dateNows)


  console.log('times ' + times)

  //if(times > "11:30"){

  var res;
  if(times > "12:00"){
    res = [resDates,dateNowz];
  }else{
    res = [dateNowz];
  }
  if(dateNows > dateYstrd){
    result = res;
  }else{
    result = ["1987-12-30"];
  }
  return result;
}

var dateCheck;

function loadDate(callback) {

$.ajax({
  url: "{!! url('getDate') !!}" + "/" + $('#id_jenis_layanan').val() + "/" + $('#quota').val() ,
  data: {},
  dataType: "json",
  type: "get",
  success: function(data) {
    // console.log(data)
    // console.log(data['hasil']);
    // console.log(data['jumlah']);

    dateCheck = data['hasil'];
    dateCheckLength = data['jumlah'];
    return callback(dateCheck);
  },
  error: function(jqXHR, textStatus, errorThrown) {
    var errorMsg = 'Ajax request failed table with: ' + errorThrown;
    console.log(errorMsg);
  }
});

}


$(document).ready(function() {
// alert("test")
readyFunction()
});

var jamKloter = 0
async function checkKloter(callback){
  var idSubLayanan = $('#id_jenis_layanan').val()
  // var dateNow = new Date(2020, 7, 4, 11, 30, 30, 0);
  var dateNow = new Date();
  // var timess = '2020-07-08';
  var timess = timeNow();
  // var time = '12:59'
  var time = dateNow.getHours()+':'+dateNow.getMinutes()

  var inputDate = $('#dateJob').val()
  var split = inputDate.split('/')
  // var dateJob = split[2]+'-'+split[0]+'-'+split[1]
  var dateJob = inputDate

  var jumlahKuota = 0
  var kuota = 0
  var bagianKuota = []
  var kuotaTerdaftar = 0

  await $.get("{{url('getTotalRegistrasi')}}/"+(timess == dateJob ? timess : dateJob)+'/'+idSubLayanan, function(data, status){
    kuotaTerdaftar = data[1]
  });
  $.ajax({
    url: "{!! url('getKloterByIdSubLayanan') !!}/"+idSubLayanan,
    data: {},
    dataType: "json",
    type: "get",
    success: function(data) {
      if(data[0] == 200){
        if(data[1]['jadwalDetilDTOList']){
          // jumlahKuota = data[1]['subLayananDTO']['kuota']
          jumlahKuota = $('#quota').val()
          $('#jumlah_kuota').val(jumlahKuota)
          $('#kuota_terdaftar').val(kuotaTerdaftar)
          var dataJadwalDetil = data[1].jadwalDetilDTOList
          var lastData = dataJadwalDetil[dataJadwalDetil.length-1]
          if(timess == dateJob){
            if(time <= lastData.jamAkhir){
              if(kuotaTerdaftar < jumlahKuota){
                $.each(dataJadwalDetil, function( index, value ) {
                  var bagian = {
                    'awal': kuota+1,
                    'akhir': parseInt(kuota)+parseInt(value.jumlahKuota)
                  }
                  bagianKuota.push(bagian)
                  kuota = kuota + parseInt(value.jumlahKuota)
                  if(time <= value.jamAkhir){
                    console.log('berhasil membagi melalui jam')
                    if(kuotaTerdaftar == 0){
                      console.log('berhasil membagi melalui kuota')
                      jamKloter = value.id
                      $('#jamKloterAwal').val(value.jamAwal)
                      $('#jamKloterAkhir').val(value.jamAkhir)
                      $('#jamKloterSekarang').val(time)
                      $('#kloter').val(value.kloter)
                      return callback()
                    }
                    if(kuotaTerdaftar >= bagianKuota[index]['awal'] && kuotaTerdaftar <= bagianKuota[index]['akhir']){
                    // if(kuotaTerdaftar <= bagianKuota[index]['akhir']){
                      jamKloter = value.id
                      $('#jamKloterAwal').val(value.jamAwal)
                      $('#jamKloterAkhir').val(value.jamAkhir)
                      $('#jamKloterSekarang').val(time)
                      $('#kloter').val(value.kloter)
                      return callback()
                    }

                  }
                });
              }else{
                $.confirm({
                  title: 'Informasi',
                  content: 'kuota di sesi ini telah habis',
                  buttons: {
                    ok: function() {},
                  }
                });
              }
            }else{
              $.confirm({
                title: 'Informasi',
                content: 'jam kloter hari ini telah selesai',
                buttons: {
                  ok: function() {},
                }
              });
            }
          } else {
            if(kuotaTerdaftar < jumlahKuota){
                $.each(dataJadwalDetil, function( index, value ) {
                  var bagian = {
                    'awal': kuota+1,
                    'akhir': parseInt(kuota)+parseInt(value.jumlahKuota)
                  }
                  bagianKuota.push(bagian)
                  kuota = kuota + parseInt(value.jumlahKuota)

                  if(kuotaTerdaftar == 0){
                    // console.log('berhasil membagi melalui kuota')
                    jamKloter = value.id
                    $('#jamKloterAwal').val(value.jamAwal)
                    $('#jamKloterAkhir').val(value.jamAkhir)
                    $('#jamKloterSekarang').val(time)
                    $('#kloter').val(value.kloter)

                    return callback()
                  }
                  if(kuotaTerdaftar >= bagianKuota[index]['awal'] && kuotaTerdaftar <= bagianKuota[index]['akhir']){
                  // if(kuotaTerdaftar <= bagianKuota[index]['akhir']){
                    // console.log(kuotaTerdaftar, bagianKuota[index]['akhir'], value.kloter)
                    // console.log('berhasil membagi melalui kuota')
                    jamKloter = value.id
                    $('#jamKloterAwal').val(value.jamAwal)
                    $('#jamKloterAkhir').val(value.jamAkhir)
                    $('#jamKloterSekarang').val(time)
                    $('#kloter').val(value.kloter)

                    return callback()
                  }
                  // else{
                  //   kuotaTerdaftar = kuotaTerdaftar+parseInt(bagianKuota[index]['akhir'])
                  // }
                });
              }else{
                $.confirm({
                  title: 'Informasi',
                  content: 'kuota di sesi ini telah habis',
                  buttons: {
                    ok: function() {},
                  }
                });
              }
          }
        }else{
          $.get("{{url('getSubLayananById')}}/"+idSubLayanan, function(data, status){
            if(data[0] == 200){
              if(kuotaTerdaftar < data[1]['kuota']){
                jamKloter = 0
                callback()
              }else{
                $.confirm({
                  title: 'Informasi',
                  content: 'kuota hari ini telah habis',
                  buttons: {
                    ok: function() {},
                  }
                });
              }
            }
          });
        }
      }else{
        $.get("{{url('getSubLayananById')}}/"+idSubLayanan, function(data, status){
            if(data[0] == 200){
              if(kuotaTerdaftar < data[1]['kuota']){
                jamKloter = 0
                callback()
              }else{
                $.confirm({
                  title: 'Informasi',
                  content: 'kuota hari ini telah habis',
                  buttons: {
                    ok: function() {},
                  }
                });
              }
            }
          });
      }
    },
    error: function(jqXHR, textStatus, errorThrown) {
      var errorMsg = 'Ajax request failed table with: ' + errorThrown;
      console.log(errorMsg);
    }
  });
}

function readyFunction(){
var timess = timeNow();
console.log("TIME NOW " + timess);
loadDate(function(dateCheck) {
// console.log(dateCheck);

var hariLibur = '{{$hariLibur}}'
var splitHariLibur = hariLibur.split(",")
// console.log(splitHariLibur)
var static_date_union = splitHariLibur
var dinamic_union_date = dateCheck;
var fungsional_union_date = timess;

var union = [...new Set([...static_date_union, ...dinamic_union_date, ...fungsional_union_date])];

$('#tanggal_merah').val(union);
// console.log(arrayables, hasilCheck1);

var rowElement = [];
$('.targetFieldRecord').each(function() {
  targetField.push($(this).val());
});

var datet = [];
var tips = [];
var dateToday = new Date();
$("#dateJob").datepicker({
  minDate: 0,
  defaultDate: "+1w",
  autoclose: true,
  dateFormat: 'yy-mm-dd',
  beforeShowDay: function(date) {
    var datestring = jQuery.datepicker.formatDate('yy-mm-dd', date);
    var weekend = $.datepicker.noWeekends
    // console.log(weekend);
    var hindex = $.inArray(datestring, datet);
    if (hindex > -1) {
      return [true, 'highlight', tips[hindex]];
    }
    console.log('datestring '+ datestring)
    var aindex = $.inArray(datestring, union);
    return [aindex == -1]
  }

});

});
}

var numFiles = 0;
function handleFileSelect(file)
{
  if (!window.File || !window.FileReader || !window.FileList || !window.Blob) {
    alert('The File APIs are not fully supported in this browser.');
    return;
  }

  var input = file;
  if (!input.files[0]) {
    $('#noteFile').show()
  }
  else {
    $('#noteFile').hide()
    for (var i = 0; i < input.files.length; i++) {
      (function(fel){
          var files = fel;
          var fr = new FileReader()
          fr.onload = (efr) => {
            $('#preview').append($('<small>')
                .text('- ')
                .append($('<a title="Klik untuk melihat" onclick="previewModal(\''+efr.target.result+'\')">')
                    .text(files.name)
                    .css('cursor', 'pointer')
                    .text(files.name)
                )
                .append('</br>')
            )
            // $('#preview').append('<small>- <a style="cursor:pointer" onclick="console.log('lonte')">'+ files.name +'</a></small></br>')
          }

          fr.readAsDataURL(files)
      })(input.files[i])
    }
  }
}

function receivedText() {
  document.getElementById('preview').appendChild(document.createTextNode(fr.result));
}

function previewModal(source){
  $('#modalPreview').modal('show')
  $('#preview-image').attr('src', source)
}
var numFiles;
var ktp;
$("input[type='file']").on("change", function(){
  $('#preview').empty()
  handleFileSelect($(this).get(0))
  numFiles = $(this).get(0).files.length
  ktp = $(this).get(0).files
});


$('#btnsubmit').on('click', function() {
if ($('#dateJob').val() == "") {
  $.confirm({
    title: 'Informasi',
    content: 'Harap mengisi Tanggal Konsultasi',
    buttons: {
      ok: function() {},
    }
  });
}
else if ($('#jenisKendala').val() == "") {
  $.confirm({
    title: 'Informasi',
    content: 'Harap mengisi Jenis Kendala',
    buttons: {
      ok: function() {},
    }
  });
}
 else {
  checkKloter(function(){
    console.log("after check")
    $('#jamKloter').val(jamKloter)
    if($('#pertanyaan').val() == "") {
      $.confirm({
        title: 'Informasi',
        content: 'Harap mengisi kolom Pertanyaan',
        buttons: {
          ok: function() {},
        }
      });
      return false
    } 
    // else if($('#filePermasalahan').val() == "") {

    //     $.confirm({
    //       title: 'Informasi',
    //       content: 'Harap mencantumkan Gambar Permasalahan',
    //       buttons: {
    //         ok: function() {},
    //       }
    //     });
    //     return false
    // } 
    else {
       if(numFiles >= 5){
         $.confirm({
           title: 'Informasi',
           content: 'Jumlah file tidak boleh lebih dari 4',
           buttons: {
             ok: function() {},
           }
         });
         return false
       }else if(numFiles < 5){
        if(numFiles != 0){
           for (var i = 0; i < ktp.length; i++) {
             var fileSize = (ktp[i].size / 1024 / 1024);
             if(fileSize > 2) {
               $.confirm({
                   title: 'Informasi',
                   content: 'Ukuran gambar tidak boleh melebihi dari 2MB',
                   buttons: {
                     ok: function() {$('#ktp').val('');},
                   }
                 });
                return false
              }
           }
         }
         console.log("coming check avail")
            $.ajax({
              url: "{!! url('check-available') !!}" + "/" + $('#email').val() + "/" + $('#dateJob').val() ,
              data: {},
              dataType: "json",
              type: "get",
              success: function(data) {
                console.log('response avail ' + data[0])
                if(data[0] == 200){
                  $.confirm({
                    title: 'Informasi',
                    content: 'Mohon maaf anda tidak bisa melakukan pendaftaran antrean karena belum melewati waktu konsultasi anda!',
                    buttons: {
                      ok: function() {$('#ktp').val('');},
                    }
                  });
                }else if(data[0] == 404){
                  loadingShow();
                  $('#fDaerah').submit();
                }else{
                  $.confirm({
                  title: 'Informasi',
                  content: 'Kesalahan pada jaringan server, harap menghubungi bantuan!',
                  buttons: {
                    ok: function() {$('#ktp').val('');},
                  }
                });
                }

              },
              error: function(jqXHR, textStatus, errorThrown) {
                var errorMsg = 'Ajax request failed table with: ' + errorThrown;
                console.log(errorMsg);
            }
          });

       }
      return false
      }
  })
}
});

</script>

@endsection
