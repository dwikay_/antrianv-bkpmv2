<!-- Modal -->
<div class="modal fade" id="modalPreview" tabindex="1" role="dialog" aria-labelledby="modalPreviewTitle" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <img src="" id="preview-image" width="100%" />
      </div>
    </div>
  </div>
</div>