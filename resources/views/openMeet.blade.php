@extends('layout')
@section('content')
<div class="row">
  <div class="col"></div>
  <div class="col-md-9" style="background-color: #fff;">
    <div class="row">
      <!-- content -->
      <div class="col-12 p-5">
        <div class="row">
        <div class="col-12">
          <img src="{{asset('home/images/logo_oss.svg')}}" class="logoTop" alt="">
        </div>
        <div class="col-md-11 pt-5">
          <h5 style="font-family: arial;" class="text-center">Tata cara Konsultasi Virtual</h5>
          <ol class="pt-3">
            <li>Aktifkan suara dan video saat melakukan konsultasi. Dianjurkan untuk menggunakan perangkat berupa komputer/laptop yang sudah terinstall software zoom versi terbaru untuk mempermudah konsultasi dan memperjelas informasi yang disampaikan. Mohon untuk menyiapkan list pertanyaan dan akses akun OSS sebelum sesi konsultasi agar sesi konsultasi dapat berjalan dengan lebih efektif dan efisien. Pastikan koneksi jaringan internet yang digunakan stabil pada saat melakukan konsultasi. </li>
            <li class="pt-3">Periksa email yang digunakan untuk melakukan pendaftaran konsultasi pada situs <a href="#" onClick="window.open('http://antrian.bkpm.go.id', '_blank')">htpp://antrian.bkpm.go.id</a> Kode unik akan dikirimkan ke alamat email tersebut pada saat anda mendapatkan konfirmasi pendaftaran antrian. Gunakan kode unik dan sesi konsultasi sebagai nama pada akun zoom anda dengan format “sesi konsultasi_kode unik” (contoh: “C_IX9653”) </li>
            <li class="pt-3">Klik tulisan tautan undangan Zoom yang dikirimkan melalui email yang didaftarkan untuk konsultasi secara virtual pada tanggal dan waktu konsultasi <b>sesuai dengan jadwal konsultasi yang diperoleh.</b> Tautan tidak dapat digunakan di luar waktu konsultasi yang telah ditentukan.</li>
            <li class="pt-3">Anda akan masuk ke dalam ruang tunggu. Mohon untuk menunggu di lobby virtual hingga petugas host konsultasi virtual selesai memverifikasi kode unik dan kesesuaian jadwal konsultasi anda sebelum memasukan anda ke main session konsultasi virtual.</li>
            <li class="pt-3">Host konsultasi virtual akan memberikan nomor antrian dengan cara merubah nama anda dengan menambahkan nomor urut konsultasi virtual (contoh: “1_C_IX9653”).</li>
            <li class="pt-3">Setelahnya, tunggu host konsultasi virtual untuk memasukan anda ke dalam Ruang Konsultasi Virtual dan menugaskan petugas FO untuk melayani anda. </li>
            <li class="pt-3">Satu sesi konsultasi virtual hanya diperuntukkan untuk satu pelaku usaha dengan durasi konsultasi maksimal <b>20 menit</b> sesuai dengan jadwal pada undangan konsultasi virtual.</li>
            <li class="pt-3">Jika koneksi terputus pada saat konsultasi virtual, anda dapat masuk kembali dengan menggunakan tautan yang sama selama durasi konsultasi virtual  anda belum berakhir dan menginformasikan kepada host konsultasi virtual untuk kembali meng-assign anda ke ruang konsultasi virtual yang telah ditentukan</li>
            <li class="pt-3">Bila ingin mengakhiri sesi konsultasi virtual silahkan klik <b>leave meeting</b>.</li>
            <li class="pt-3">Setelah melakukan konsultasi tatap muka secara virtual, mohon dapat mengisi daftar hadir dan survei pelaksanaan konsultasi melalui tautan berikut: <a href="#" onClick="window.open('http://bit.ly/IKMPELAYANANBKPM', '_blank')">http://bit.ly/IKMPELAYANANBKPM</a></li>
          </ol>
        </div>
        <div class="col-md-11 pt-5">
  				<label>Terima kasih telah menggunakan sistem Antrean Online Kementrian Investasi/BKPM.</label><br>
  				<label class="pt-3">Silahkan mengakses pada tautan link konsultasi virtual terlampir sesuai dengan tanggal yang di pilih, sebagai berikut:</label><br><br>
          <a href="#" onClick="window.open('{{$paramLinkVirtual['linkVirtual']}}', '_blank')" class="btn btn-md text-center btn-primary" style="margin-left:50%"><span class="fa fa-video-camera"></span>Konsultasi Virtual</a>
          <br>
          <label class="pt-5">Atas perhatian bapak/ibu kami ucapkan terima kasih.</label><br>
          <label class="">©️ Kementrian Investasi/BKPM</label>
  			</div>
        </div>
      </div>
    </div>
  </div>
  <div class="col"></div>
</div>
@endsection
@section('script')
<script>

loadingHide();

function loadingShow() {
$('#loading').css('opacity', '0.7');
$('#loading').css('background-color', '#000');
$('#loading-image').css('display', 'block');
$('#loading').css('display', 'block');
}

function loadingHide() {
$('#loading').css('opacity', '0');
$('#loading').css('background-color', 'as');
$('#loading-image').css('display', 'none');
$('#loading').css('display', 'none');
}

</script>
@endsection
