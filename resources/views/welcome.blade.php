
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <!-- <meta name="viewport" content="width=device-width, initial-scale=1"> -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{asset('home/css/bootstrap5.css')}}" >
    <link rel="stylesheet" type="text/css" href="{{asset('home/css/custom.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('home/css/all.css')}}">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/solid.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
    <link rel="stylesheet" href="{{asset('registrasi/css/jquery-confirm.css')}}">

	<style>
		@import url('https://fonts.googleapis.com/css2?family=Montserrat&display=swap');
	</style>

    <title>Sistem Perizinan Berusaha Terintegrasi Secara Elektronik</title>
  </head>
  <body class="d-flex flex-column h-100">

    	<div class="container bd mb-5">
    		<div class="row">
    			<div class="col-12" style="margin-bottom: 5%;">
    				<img src="{{asset('home/images/logo_oss.svg')}}" class="logoTop" alt="">
    			</div>

    			<div class="col-md-6 p-5 mt-5">
    				<form>
					  <div class="mb-3">
					    <label for="exampleInputEmail1" class="form-label">Metode Konsultasi</label>
					    <select class="form-select" aria-label="Default select example" id="tipe" name="tipe">
						  <option value="ots">Tatap Muka</option>
						  <option value="virtual" selected>Virtual</option>
						</select>
					  </div>

					  <div class="mb-3">
					    <label for="exampleInputPassword1" class="form-label">Layanan Konsultasi</label>
					    <select class="form-select" aria-label="Default select example" name="subLayanan" id="subLayanan">
						        @foreach($layanan as $layanans)
                    <option value="{{$layanans['id']}}">{{$layanans['namaLayanan']}}</option>
                    @endforeach
						</select>
					  </div>
					  <a href="javascript:void(0)" id="btnCheck" class="btn btn-primary">Kirim</a>
					</form>
    			</div>
          <div class="col-md-6" style="position: relative;">
            <div class="v-line"></div>
    				<h4>TATA CARA PENDAFTARAN <br> KONSULTASI</h4>
    				<div class="scroll-line pt-4">
    					<ul>
                <li>Konsultasi virtual dapat diberikan kepada pemilik Hak Akses sesuai dengan username atau email  yang digunakan untuk MASUK.</li><br>
                <li>Anda dapat memilih tanggal dan sesi konsultasi sesuai dengan ketersediaan kuota konsultasi per hari dan kebutuhan Anda.</li><br>
                <li>Anda diminta untuk menuliskan kendala sesuai dengan kegiatan usaha, badan usaha, atau instansi yang terdaftar pada Hak Akses sebagaimana dimaksud di atas. Anda tidak diperkenankan konsultasi di luar kegiatan tersebut.</li><br>
                <li>Agar konsultasi berjalan secara efektif dan efisien Anda diminta untuk mengunggah tangkapan layar komputer/laptop yang menjelaskan secara detail kendala yang dihadapi.<br><br>
                  <input type="checkbox" name="checkTerms" id="checkTerms"><b> Saya telah membaca dan menyetujui ketentuan di atas.</b>
                </li><br>
              </ul>
    				</div>

    			</div>
    		</div>
    	</div>
    	<footer class="footer mt-auto">
    	<div class="container-fluid">
    		<div class="row">
    			<div class="col-12 fore p-4">
    				<div class="row">
    					<div class="col-md-4">
    						<img src="{{asset('home/images/bkpm_white.svg')}}" class="ft-logo" alt="">
    						<small class="copy mt-2 mb-5">Jl. Jenderal Gatot Subroto <br>No.44, Jakarta 12190 Indonesia</small>
    						<small class="copy">© 2021 Lembaga OSS - Kementerian Investasi/BKPM</small>
    					</div>
    					<div class="col-md-4">
    						<small class="footer-text mb-4">Sistem Perizinan Berusaha Terintegrasi Secara Elektronik</small>
    						<small class="copy">Kontak kami:</small>
    						<button class="btn-sos"><i class="fas fa-phone"></i></button> <small class="btn-stext">169</small>
    						<button class="btn-sos"><i class="fas fa-envelope "></i></button> <small class="btn-stext">kontak@oss.go.id</small>
    						<small class="copy mt-3 mb-3">Ikuti Lembaga OSS di Media Sosial:</small>
    						<button class="btn-sos"><i class="fas fa-instagram"></i></button>
    						<button class="btn-sos"><i class="fas fa-facebook"></i></button>
    						<button class="btn-sos"><i class="fas fa-youtube"></i></button>
    						<button class="btn-sos"><i class="fas fa-twitter"></i></button>
    					</div>
    				</div>
    			</div>
    		</div>
    	</div>
    	</footer>

  <script src="{{asset('registrasi/js/jquery.min.js')}}" crossorigin="anonymous"></script>
  <script src="{{asset('registrasi/js/bootstrap4.min.js')}}"></script>
  <script src="{{asset('admin/js/popper.min.js')}}"></script>
  <script src="{{asset('registrasi/js/jstnumber.js')}}"></script>
  <script src="{{asset('registrasi/js/jquery-confirm.js')}}"></script>
  <script src="{{asset('registrasi/js/sweetalert.min.js')}}"></script>

  <script>

  $('#btnCheck').on('click', function(){

if(document.getElementById('checkTerms').checked == false){
     $.confirm({
              title: 'Informasi',
              content: 'Harap menyetujui ketentuan yang tertera',
              buttons: {
                  ok: function () {
                  },
              }
          });
    }

    else {
      location.href="{{url('verifyToken')}}/" + $('#keyToken').val() + "/" + $('#subLayanan').val() + "/" + $('#tipe').val()
  }
});

  </script>

  </body>
</html>
