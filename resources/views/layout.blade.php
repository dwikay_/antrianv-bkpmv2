
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <!-- <meta name="viewport" content="width=device-width, initial-scale=1"> -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{asset('form/css/bootstrap5.css')}}" >
    <!-- <link rel="stylesheet" href="{{asset('bootstrap4/css/bootstrap.min.css')}}" > -->
    <link rel="stylesheet" href="{{asset('datetimepicker/bootstrap-datetimepicker.min.css')}}">
    <link rel="stylesheet" href="{{asset('jqueryui/jquery-ui.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('form/css/custom.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('form/css/all.css')}}">
    <link rel="stylesheet" href="{{asset('registrasi/css/jquery-confirm.css')}}">
    <link rel="stylesheet" href="{{asset('registrasi/css/sweetalert.min.css')}}">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/solid.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
<!--     <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/tempusdominus-bootstrap-4/5.38.0/css/tempusdominus-bootstrap-4.min.css" crossorigin="anonymous" />
 -->

	<style>
		@import url('https://fonts.googleapis.com/css2?family=Montserrat&display=swap');

	</style>

  <style type="text/css" media="screen">

			#ui-datepicker-div{z-index: 100 !important;}
			.copy{
				color: #fff;
			}

			.date_nonaktif td a {
      background-color: red;
      color: white;
    }

    .ui-datepicker-unselectable .ui-state-default {
      background-color: red;
      opacity: 1;
      color: #fff;
    }

    th.ui-datepicker-week-end,
    td.ui-datepicker-week-end {
      display: none;
    }

    .overlay {
      background: rgba(0,0,0,0.5);
    }


    #loading {
       width: 100%;
       height: 100%;
       top: 0;
       left: 0;
       position: fixed;
       display: block;
       /* opacity: 0.7;
       background-color: #fff; */
       z-index: 99;
       text-align: center;
    }

    #loading-image {
        position: absolute;
        color: #FFF;
        font-size: 20px;
        top: 350px;
        left: 48%;
        z-index: 100;
    }

    .se-pre-con {
            position: fixed;
            left: 0px;
            top: 0px;
            width: 100%;
            height: 100%;
            background:#000;opacity:0.4;filter:alpha(opacity=40);
            z-index: 9999;
            background: center no-repeat #000;
        }

		</style>

    <title>Pendaftaran Konsultasi</title>
  </head>
  <body class="d-flex flex-column h-100">

    	<div class="container">
    		@yield('content')
    	</div>
      <div>
        <div id="loading">
          <div id="loading-image"alt="Loading...">Memuat halaman...</div>
        </div>
      </div>
    	<footer class="footer mt-auto">
    	<div class="container-fluid">
    		<div class="row">
    			<div class="col-12 fore p-4">
    				<div class="row">
    					<div class="col-md-4">
    						<img src="{{asset('form/images/bkpm_white.svg')}}" class="ft-logo" alt="">
    						<small class="copy mt-2 mb-5">Jl. Jenderal Gatot Subroto <br>No.44, Jakarta 12190 Indonesia</small>
    						<small class="copy">© 2021 Lembaga OSS - Kementerian Investasi/BKPM</small>
    					</div>
    					<div class="col-md-4">
    						<small class="footer-text mb-4">Sistem Perizinan Berusaha Terintegrasi Secara Elektronik</small>
    						<small class="copy">Kontak kami:</small>
    						<button class="btn-sos"><i class="fas fa-phone"></i></button> <small class="btn-stext">169</small>
    						<button class="btn-sos"><i class="fas fa-envelope "></i></button> <small class="btn-stext">kontak@oss.go.id</small>
    						<small class="copy mt-3 mb-3">Ikuti Lembaga OSS di Media Sosial:</small>
                <button class="btn-sos"><i class="fab fa-instagram"></i></button>
    						<button class="btn-sos"><i class="fab fa-facebook"></i></button>
    						<button class="btn-sos"><i class="fab fa-youtube"></i></button>
    						<button class="btn-sos"><i class="fab fa-facebook-square"></i></button>
    					</div>
    				</div>
    			</div>
    		</div>
    	</div>
    	</footer>

    	<!-- Modal -->
			<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
			  <div class="modal-dialog">
			    <div class="modal-content">
			      <div class="modal-header">
			        <h5 class="modal-title" id="exampleModalLabel" style="text-align:center; display: block; width: 100%;">Terima Kasih</h5>
			      </div>
			      <div class="modal-body" style="text-align: center;">
			        <h5>Anda telah terdaftar pada tanggal</h5>
			        <h4>02-Ags-2021 OSS A (08:00 - 09:00)</h4>
			        <h5>silahkan cek email anda untuk melihat email konfirmasi</h5>
			      </div>
			    </div>
			  </div>
			</div>

      <script src="{{asset('registrasi/js/jquery.min.js')}}" crossorigin="anonymous"></script>
      <script src="{{asset('jqueryui/jquery-ui.min.js')}}"></script>
      <script src="{{asset('registrasi/js/bootstrap4.min.js')}}"></script>
      <script src="{{asset('registrasi/js/jstnumber.js')}}"></script>
      <script src="{{asset('registrasi/js/jquery-confirm.js')}}"></script>
      <script src="{{asset('registrasi/js/sweetalert.min.js')}}"></script>
      <script src="{{asset('registrasi/js/jstnumber.js')}}"></script>
      <script src="{{asset('datetimepicker/bootstrap-datetimepicker.min.js')}}"></script>
      <script src="https://momentjs.com/downloads/moment.min.js"></script>

      @yield('script')

  </body>
</html>
