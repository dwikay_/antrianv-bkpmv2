
<!doctype html>
<html lang="en">
  <head>
	<style>
		@import url('https://fonts.googleapis.com/css2?family=Montserrat&display=swap');
	</style>
    <title>Konfirmasi Antrian</title>
  </head>
  <body style="font-family: 'Montserrat', sans-serif;background:#dfe6e9">
  	<div style="width:700px; display:block; padding:0px 10px; padding-top: 20px; margin:0 auto;">
      <div style="width:100%; height: 80px; display: display: block;">
  			<img src="{{asset('LOGO_OSS_NEW_2.b9df44a.png')}}" style="width:180px; display: block;" alt="">
  		</div>
  	</div>
  	<!-- Container utama -->
  	<div style="background: #ffff;width: 700px; min-height: 400px; padding: 30px; border-radius: 40px; display: block; margin:0 auto; box-shadow: 1px 0px 85px 17px rgba(0,0,0,0.15); -webkit-box-shadow: 1px 0px 85px 17px rgba(0,0,0,0.15);
    -moz-box-shadow: 1px 0px 85px 17px rgba(0,0,0,0.15); margin-bottom: 30px;">
  		<!-- container logo -->
  		<!-- container judul -->
  		<h3 style="display: block; padding: 0px 50px; font-weight:normal; font-size:15px;">Terima kasih <b>{{$registrasi['namaDireksi']}}</b> telah mebuat janji konsultasi virtual. Berikut ini adalah ringkasan data pendaftaran Anda</h3>
  		<!-- tabel isi -->
  		<table style="padding: 0px 30px; font-size: 14px; width:100%;" cellpadding="10" cellspacing="2">
  			<tr>
  				<td style="background-color: #f7f7f7; border-top-left-radius: 40px; border-bottom-left-radius: 40px;"><span style="font-weight: bold;">Nama Badan Usaha</span></td>
  				<td style="background-color: #f7f7f7; border-top-right-radius: 40px; border-bottom-right-radius: 40px;"><span>{{$registrasi['namaBadanUsaha']}}</span></td>
  			</tr>
  			<tr>
  				<td style="background-color: #f7f7f7; border-top-left-radius: 40px; border-bottom-left-radius: 40px;" ><span style="font-weight: bold;">Nama Direksi/Pengurus</span></td>
  				<td style="background-color: #f7f7f7; border-top-right-radius: 40px; border-bottom-right-radius: 40px;" ><span>{{$registrasi['namaDireksi']}}</span></td>
  			</tr>
        <tr>
  				<td style="background-color: #f7f7f7; border-top-left-radius: 40px; border-bottom-left-radius: 40px;" ><span style="font-weight: bold;">NIK/Paspor Direksi/Pengurus</span></td>
  				<td style="background-color: #f7f7f7; border-top-right-radius: 40px; border-bottom-right-radius: 40px;" ><span>{{$registrasi['nik']}}</span></td>
  			</tr>
  			<tr>
  				<td style="background-color: #f7f7f7; border-top-left-radius: 40px; border-bottom-left-radius: 40px;" ><span style="font-weight: bold;">Tanggal Konsultasi</span></td>
  				<td style="background-color: #f7f7f7; border-top-right-radius: 40px; border-bottom-right-radius: 40px;" ><span>{{$waktu_pengambilan}}</span></td>
  			</tr>
  			<tr>
  				<td style="background-color: #f7f7f7; border-top-left-radius: 40px; border-bottom-left-radius: 40px;"><span style="font-weight: bold;">Sesi Konsultasi</span></td>
  				<td style="background-color: #f7f7f7; border-top-right-radius: 40px; border-bottom-right-radius: 40px;"><span>Sesi {{$registrasi['jamKloter']}}</span></td>
  			</tr>
  			<tr>
  				<td style="background-color: #f7f7f7; border-top-left-radius: 40px; border-bottom-left-radius: 40px;"><span style="font-weight: bold;">Kode Unik</span></td>
  				<td style="background-color: #f7f7f7; border-top-right-radius: 40px; border-bottom-right-radius: 40px;"><span>{{$registrasi['kodeKonfirmasi']}}</span></td>
  			</tr>
  			<tr>
  				<td style="background-color: #f7f7f7; border-top-left-radius: 40px; border-bottom-left-radius: 40px;"><span style="font-weight: bold;">Kendala</span></td>
  				<td style="background-color: #f7f7f7; border-top-right-radius: 40px; border-bottom-right-radius: 40px;"><span>{{$registrasi['pertanyaan']}}</span></td>
  			</tr>
  		</table>

  		<!-- container info -->
  		<div style="display:block; margin-top: 30px; padding:0px 30px;">
  			<span style="text-align: justify; line-height: 1.5;">
  				Silahkan menekan tombol di bawah ini untuk mengakses tautan konsultasi virtual sesuai dengan jadwal konsultasi di atas
  			</span>
  		</div>

  		<!-- button link -->
  		<div style="display:block; padding:0px 30px; margin-top:30px;">
  				<a href="{{url('open-meet/'.$registrasi['id'])}}" style="display: block; border-radius:40px; background-color: #00479b; color:#fff; text-align:center; text-decoration: none; padding:10px;">Konsultasi Virtual</a>
  		</div>

  		<!-- container info -->
      <div style="display:block; margin-top: 30px; padding:0px 30px;">
  			<span style="text-align: justify; line-height: 1.5;">
  				Pastikan Anda telah memahami tata cara konsultasi pada <a href="https://bit.ly/tatacarakonsultasiOSSvirtual">tautan ini</a>.
          <br><br>Setelah Anda mengikuti sesi konsultasi virtual,
          kami mohon partisipasi Anda dengan mengisi kuesioner pada <a href="http://bit.ly/IKMPELAYANANBKPM">tautan ini</a>
          untuk meningkatkan kualitas pelayanan publik Lembaga OSS.
  			</span>
  		</div>

  		<!-- container info -->
  		<div style="display:block; margin-top: 30px; padding:0px 30px;">
  			<span style="text-align: justify; line-height: 1.5;">
  				<b>Salam,</b> <br> Lembaga OSS - Kementerian Investasi/BKPM
  			</span>

        <table cellspacing="5" style="margin-top: 20px;">
  				<tr>
  					<td><img src="{{asset('icon/icon_03.png')}}" style="width: 30px; height: 35px; display: block;" alt=""></td>
  					<td><span>169</span></td>
  				</tr>
  				<tr>
  					<td><img src="{{asset('icon/icon_07.png')}}" style="width: 30px; height: 22px; display: block;" alt=""></td>
  					<td><span>kontak@oss.go.id</span></td>
  				</tr>
  				<tr>
  					<td><img src="{{asset('icon/icon_9.png')}}" style="width: 30px; height: 35px; display: block;" alt=""></td>
  					<td><span>Jalan Jenderal Gatot Subroto No. 44 <br> Jakarta 12190 Indonesia</span></td>
  				</tr>
  			</table>

        <table style="margin:0 auto; margin-top:30px; width:80%;">
  				<tr>
  					<td style="width:100px;"><a href="" style="background-image: url({{asset('icon_13.png')}}); background-repeat: no-repeat; float: left; background-size: cover; width: 20px; height: 20px; display: block;"><span style="display:block; padding-left:30px; text-decoration:none !important; font-size: 13px;">oss.go.id</span></a></td>
  					<td><a href="" style="background-image: url({{asset('icon_15.png')}}); background-repeat: no-repeat; float: left; background-size: cover; width: 20px; height: 20px; display: block;"><span style="display:block; padding-left:30px; text-decoration:none !important; font-size: 13px; width:120px;">OSS Indonesia</span></a></td>
  					<td><a href="" style="background-image: url({{asset('icon_17.png')}}); background-repeat: no-repeat; float: left; background-size: cover; width: 20px; height: 20px; display: block;"><span style="display:block; padding-left:30px; text-decoration:none !important; font-size: 13px;;">@OSS_id</span></a></td>
  					<td><a href="" style="background-image: url({{asset('icon_19.png')}}); background-repeat: no-repeat; float: left; background-size: cover; width: 20px; height: 20px; display: block;"><span style="display:block; padding-left:30px; text-decoration:none !important; width: 120px; font-size: 13px;;">OSS Indonesia</span></a></td>
  				</tr>
  			</table>
  		</div>

  		<!-- line separator -->



  	</div>
  	<div style="width: 100%; margin-top: 50px; margin: 0 auto; height: 40px; display: block; background-color: #6F0F0B;"></div>

  </body>
</html>
