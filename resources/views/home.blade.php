@extends('layout')
@section('content')

<div class="row">
  <div class="col"></div>
  <div class="col-md-9" style="background-color: #fff;">
    <div class="row">
      <!-- content -->
      <div class="col-12 p-5">
        <div class="row">
        <div class="col-12">
          <img src="https://oss.go.id/_nuxt/img/LOGO_OSS_NEW_2.b9df44a.svg" class="logoTop" alt="">
        </div>
        <div class="col-12 mt-3 mb-5 pt-5">
        <!-- Judul kontent -->
          <h4>Sistem Antrean Perizinan Berusaha <br>Terintegrasi Secara Elektronik</h4>
          <label style="margin-top:5%">Saat ini konsultasi virtual disediakan untuk pemilik Hak Akses OSS Berbasis Risiko. Silakan masuk atau daftar apabila belum memiliki hak akses.</label>
        </div>
        <div class="d-grid gap-2 col-12 mx-auto">
            <a href="https://ui-login.oss.go.id/login" style="width:30%;margin-left:33%" class="btn btn-primary border-0" >Masuk</a>
        </div>
        <div class="d-grid gap-2 col-12 mx-auto pt-3">
            <a href="https://ui-login.oss.go.id/login?action=register" style="width:30%;margin-left:33%" class="btn btn-danger border-0" >Daftar</a>
        </div>

        </div>
      </div>
    </div>
  </div>
  <div class="col"></div>
</div>

@endsection
@section('script')
<script>

loadingHide();

function loadingShow() {
$('#loading').css('opacity', '0.7');
$('#loading').css('background-color', '#000');
$('#loading-image').css('display', 'block');
$('#loading').css('display', 'block');
}

function loadingHide() {
$('#loading').css('opacity', '0');
$('#loading').css('background-color', 'as');
$('#loading-image').css('display', 'none');
$('#loading').css('display', 'none');
}

</script>
@endsection
